const multer = require('multer')
const fs = require('fs')
const path = './images'



fs.access(path, (error) => {
    if (error) {
        fs.mkdir(path, (error) => {
            if (error) {
                console.log(error);
            } else {
                console.log("Nouveau dossier Images créé avec succès !");
            }
        })
    } else {
        console.log("Le dossier Images existe déjà !");
    }
})


const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, path)
    },
    filename: (req, file, callback) => {
        const name = file.originalname.split(' ').join('_')
        const extArray = name.split('.')
        const extension = extArray[extArray.length -1]
        const baseName = name.replace(`.${extension}`, '')
        callback(null, `${baseName}${Date.now()}.${extension}`)
    }
})

module.exports = multer({storage: storage}).single('image')