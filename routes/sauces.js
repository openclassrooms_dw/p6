const express = require('express')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer-config')
const saucesCtrl = require('../controllers/sauces')
const router = express.Router()

router.post('/', auth, multer, saucesCtrl.createSauce)
router.put('/:id', auth, multer, saucesCtrl.modifySauce)
router.delete('/:id', auth, multer, saucesCtrl.deleteSauce)
router.get('/:id', auth, saucesCtrl.getOneSauce)
router.get('/', auth, saucesCtrl.getAllSauce)
router.post('/:id/like', auth, saucesCtrl.likeDislikeSauce)

module.exports = router;